package com.Linus.LinusChatBot;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class LinusController {
	
	@RequestMapping("/users")
    public List<User> getUsers() {
		
		Connection con=null;
		List<User> list=new ArrayList<>();
		
		//////db service////
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			con=DriverManager.getConnection("jdbc:mysql://hackathon2018.cp19kzpsbwaf.us-west-2.rds.amazonaws.com:3306/Linusdb","HackathonApr2018","Tower42017");  
			 
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from User");  
			while(rs.next()){ 
				User user=new User();
				user.setUserID(rs.getString("UserId"));
				user.setUserName(rs.getString("UserName"));
				user.setEmailID(rs.getString("EmailId"));
				list.add(user);
				System.out.println(list);
			}
			 
			}catch(Exception e){ 
				System.out.println(e);
			} 
		finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
			  
		
		
		
		
		
        return list;
    }
	
	@RequestMapping("/categories")
    public List<ChatCategory> getCategories() {
		Connection con=null;
		List<ChatCategory> list=new ArrayList<>();
		
		//////db service////
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			con=DriverManager.getConnection("jdbc:mysql://hackathon2018.cp19kzpsbwaf.us-west-2.rds.amazonaws.com:3306/Linusdb","HackathonApr2018","Tower42017");  
			 
			Statement stmt=con.createStatement();  
			ResultSet rs=stmt.executeQuery("select * from Categaries");  
			while(rs.next()){ 
				ChatCategory cat1=new ChatCategory();
				cat1.setCategoryID(rs.getString("CategaryId"));
				cat1.setCategoryName(rs.getString("CategaryName"));
				list.add(cat1);
				System.out.println(list);
			}
			 
			}catch(Exception e){ 
				System.out.println(e);
			} 
		finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
			  
		
		
		
		
		
        return list;
    }
	
	
	
	@RequestMapping("/messages/{categoryId}")
    public List<Message> getMessages(@PathVariable("categoryId") String categoryId) {
		Connection con=null;
		List<Message> list=new ArrayList<>();
		
		//////db service////
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
			con=DriverManager.getConnection("jdbc:mysql://hackathon2018.cp19kzpsbwaf.us-west-2.rds.amazonaws.com:3306/Linusdb","HackathonApr2018","Tower42017");  
			 
			//Statement stmt=con.createStatement();
			String query="select m.Message , u.UserName,  m.PostDate from Messages m, User u  where m.UserId=u.UserId  and m.CategaryId=?";
			PreparedStatement ps= con.prepareStatement(query);
			ps.setString(1, categoryId);
			ResultSet rs=ps.executeQuery();  
			while(rs.next()){ 
				Message msg=new Message();
				msg.setMessage(rs.getString("Message"));
				msg.setUserName(rs.getString("UserName"));
				msg.setPostDate(rs.getTimestamp("PostDate").toString());
				
				list.add(msg);
				System.out.println(list);
			}
			 
			}catch(Exception e){ 
				System.out.println(e);
			} 
		finally{
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
		}
		return list;
        
		}
	
        @RequestMapping( value="/messages{id}", method=RequestMethod.POST)
        public List<Message> postMessages(@RequestBody Message message) {
        
        Connection con1=null;
		List<Message> list1=new ArrayList<>();
		
		
			//////db service////
			try{  
				Class.forName("com.mysql.jdbc.Driver");  
				con1=DriverManager.getConnection("jdbc:mysql://hackathon2018.cp19kzpsbwaf.us-west-2.rds.amazonaws.com:3306/Linusdb","HackathonApr2018","Tower42017");  
				 
				Statement stmt=con1.createStatement();  
				ResultSet rs=stmt.executeQuery("select m.Message , u.UserName, c.CategaryName, m.PostDate from Messages m, User u, Categaries c  where m.UserId=u.UserId  and m.CategaryId=c.CategaryId ;");  
				while(rs.next()){ 
					Message msg=new Message();
					msg.setMessage(rs.getString("Message"));
					msg.setUserName(rs.getString("UserName"));
					msg.setCategoryName(rs.getString("CategaryName"));
					msg.setPostDate(rs.getTimestamp("PostDate").toString());
					
					list1.add(msg);
					System.out.println(list1);
				}
				 
				}catch(Exception e){ 
					System.out.println(e);
				} 
			finally{
				try {
					con1.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			
			}
			
			//return userService.createUser(user);
			return list1;
        
    }
}
