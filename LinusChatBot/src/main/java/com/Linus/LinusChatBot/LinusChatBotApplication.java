package com.Linus.LinusChatBot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("*")
@SpringBootApplication
//@ComponentScan
public class LinusChatBotApplication {

	public static void main(String[] args) {
		SpringApplication.run(LinusChatBotApplication.class, args);
	}
}
